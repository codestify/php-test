# ukfast/pokédex
Worked on project while watching England team's final. The game might have had an effect on my coding 
and choices i made 😒.

## How to setup the project
Clone the repository to your local machine and ensure that you can load `public/index.php` using a local server. Once project
cloned then please `cd` into relevant folder and run composer install.

## Project Features
 - PSR-4 Autoloading 
 - Blade templating language
 - Symfony components
 - results caching through symfony http client
 
 ## Project Structure
 - `index.php` is basically working as the project's bootstrap.
 - `src/app/` holds the main configuration and service files.
 - `src/resources` holds the view files.
 - `storage` folder has cached api results.
 - `cache` folder has view cache files.
 
 ## self code review
 - As I am applying for Senior PHP Engineer job, had to do it in PHP.
 - DRY principle has been completely violated
 - Code is tightly coupled
 - No proper error management
 - No extra effort has been put in for styling
 
 ## what went well ?
 - Loved bringing different components together after long time
 - helped me to stay on my seat and watch World cup match. Had it been not for
   this task, i would have been very stressed 😓 
  
  Any questions are welcome !! Thank you

