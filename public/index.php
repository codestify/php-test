<?php

$loader = require '../vendor/autoload.php';
$loader->register();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Lib\Framework\Core;
use Jenssegers\Blade\Blade;

$request = Request::createFromGlobals();

$views = dirname(__FILE__). DIRECTORY_SEPARATOR .'../src/resources/views';
$cache = dirname(__FILE__). DIRECTORY_SEPARATOR .'../src/cache';
$storage_path = dirname(__FILE__). DIRECTORY_SEPARATOR .'../src/storage';

$blade = new Blade($views, $cache);

// Our Framework is now handling itself the request
$app = new Core;

require dirname(__FILE__). DIRECTORY_SEPARATOR . '../src/routes/web.php';

$response = $app->handle($request);

// $response->send();