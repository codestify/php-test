<?php

namespace App\Services;


use App\Config;
use Illuminate\Support\Str;
use Symfony\Component\HttpClient\CachingHttpClient;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ApiClient
{
    /** @var ApiClient\Client */
    protected $client;

    /**
     * PokeApi constructor.
     *
     * @param $storage_path
     */
    public function __construct($storage_path)
    {
        $client = HttpClient::create([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accepts' => 'application/json'
            ]
        ]);
        $storage = new Store($storage_path);
        $this->client = new CachingHttpClient($client, $storage);
    }

    /**
     * @param $url
     * @param string $method
     * @param $limit
     * @return mixed|string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function get($url, $limit = null, $method = 'GET')
    {
        try {
            $response = $this->client
                ->request($method, $this->buildUrl($url, $limit))
                ->getContent();

            return json_decode($response, true);
        } catch (TransportExceptionInterface $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $url
     * @param null $limit
     * @return string
     */
    protected function buildUrl($url, $limit = null): string
    {
        if (Str::startsWith($url, 'https://')) {
            return $url;
        }
        if ($limit) {
            return Config::POKE_API_BASE_URL . '/' . $url . '?limit=' . $limit;
        }
        return Config::POKE_API_BASE_URL . '/' . $url;
    }
}