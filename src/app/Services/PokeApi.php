<?php

namespace App\Services;


class PokeApi
{
    /** @var ApiClient */
    protected $apiClient;

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function getList($url, $limit = 42)
    {
        try {
            $apiResults = $this->apiClient->get($url, $limit);
            $results = array_map(function ($result) {
                $pokemon = $this->apiClient->get($result['url']);
                return json_decode(json_encode($pokemon));
            }, $apiResults['results']);
            unset($apiResults['results']);
            $apiResults['results'] = $results;
            return $apiResults;
        } catch (ClientExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        }
    }

    public function getSingle($url, $id)
    {
        try {
            $pokemon = $this->apiClient->get("{$url}/{$id}");
            return json_decode(json_encode($pokemon));
        } catch (ClientExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        }
    }

    public function getByName($url, $name)
    {
        try {
            $pokemon = $this->apiClient->get("{$url}/{$name}");
            return json_decode(json_encode($pokemon));
        } catch (ClientExceptionInterface $e) {
        } catch (RedirectionExceptionInterface $e) {
        } catch (ServerExceptionInterface $e) {
        }
    }
}