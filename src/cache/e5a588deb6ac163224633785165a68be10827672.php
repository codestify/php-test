<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pokedox</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>

<!-- Page Content -->
<div class="container">
    <div class="my-5">
        <a href="/" class="btn btn-primary">Go Back</a>
    </div>
    <div class="row">
        <div class="col-md-7 offset-md-3">
            <div class="card">
                <h5 class="card-header text-center"><?php echo e($pokemon->name); ?></h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3 align-middle">
                            <img src="<?php echo e($pokemon->sprites->front_default); ?>" alt="<?php echo e($pokemon->name); ?>">
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <div>
                                    <?php $__currentLoopData = $pokemon->types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <button class="btn btn-outline-primary btn-sm"><?php echo e($type->type->name); ?></button>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <div class="pt-3">
                                    <?php $__currentLoopData = $pokemon->stats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p class="d-flex justify-content-between">
                                            <span><?php echo e($stat->stat->name); ?> </span>
                                            <span class="font-weight-bold"><?php echo e($stat->base_stat); ?></span>
                                        </p>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="container">
                        <h5 class="mt-2 mb-3">Profile</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="d-flex justify-content-between">
                                    <span>Height:</span>
                                    <span class="font-weight-bold pl-3"><?php echo e($pokemon->height/10); ?> m</span>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p class="d-flex justify-content-between">
                                    <span>Weight:</span>
                                    <span class="font-weight-bold pl-3"><?php echo e($pokemon->weight/10); ?> kg</span>
                                </p>
                                <p class="d-flex justify-content-between">
                                    <span>Abilities:</span>
                                    <span class="text-sm pl-3">
                                        <?php $__currentLoopData = $pokemon->abilities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ability): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <?php echo e($ability->ability->name); ?>,
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html><?php /**PATH /Users/alishah/www/php-test/src/resources/views/show.blade.php ENDPATH**/ ?>