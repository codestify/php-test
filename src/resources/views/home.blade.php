<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pokedox</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>

<!-- Page Content -->
<div class="container">
    <div class="my-5">

        <div class="col-md-8 offset-md-2">
            <form action="/search" method="GET">
                <div class="input-group mb-3">
                    <input type="text"
                           name="name"
                           class="form-control" placeholder="Pokemon's name" aria-label="Pokemon's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-primary" type="submit">Search</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
    <div class="card-deck mt-5 shadow">
        @foreach($pokemons['results'] as $pokemon)

            <div class="card">
                <a href="/pokemon/{{ $pokemon->id }}">
                    <img class="card-img-top img-thumbnail " src="{{ optional($pokemon->sprites)->front_default }}" alt="{{ $poken->name }}">
                    <h5 class="text-sm text-center py-1">{{ $pokemon->name }}</h5>
                </a>
            </div>
            @if ($loop->iteration % 7 === 0)
            </div>
            <div class="card-deck mt-3">
            @endif
        @endforeach
    </div>
</div>

</body>
</html>