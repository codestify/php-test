<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pokedox</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>

<!-- Page Content -->
<div class="container">
    <div class="my-5">
        <a href="/" class="btn btn-primary">Go Back</a>
    </div>
    <div class="row">
        <div class="col-md-7 offset-md-3">
            <div class="card">
                <h5 class="card-header text-center">{{ $pokemon->name }}</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3 align-middle">
                            <img src="{{ $pokemon->sprites->front_default }}" alt="{{ $pokemon->name }}">
                        </div>
                        <div class="col-sm-6">
                            <div class="">
                                <div>
                                    @foreach($pokemon->types as $type)
                                        <button class="btn btn-outline-primary btn-sm">{{ $type->type->name }}</button>
                                    @endforeach
                                </div>
                                <div class="pt-3">
                                    @foreach($pokemon->stats as $stat)
                                        <p class="d-flex justify-content-between">
                                            <span>{{ $stat->stat->name }} </span>
                                            <span class="font-weight-bold">{{ $stat->base_stat }}</span>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="container">
                        <h5 class="mt-2 mb-3">Profile</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="d-flex justify-content-between">
                                    <span>Height:</span>
                                    <span class="font-weight-bold pl-3">{{ $pokemon->height/10 }} m</span>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p class="d-flex justify-content-between">
                                    <span>Weight:</span>
                                    <span class="font-weight-bold pl-3">{{ $pokemon->weight/10 }} kg</span>
                                </p>
                                <p class="d-flex justify-content-between">
                                    <span>Abilities:</span>
                                    <span class="text-sm pl-3">
                                        @foreach($pokemon->abilities as $ability)
                                           {{ $ability->ability->name }},
                                        @endforeach
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>