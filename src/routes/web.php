<?php

$apiClient =  new \App\Services\ApiClient($storage_path);
$pokeApiService = new \App\Services\PokeApi($apiClient);

$app->map('/', function () use ($blade, $pokeApiService) {
    $pokemons = $pokeApiService->getList('pokemon');
    echo $blade->make('home' , compact('pokemons'));
});

$app->map('/pokemon/{id}', function ($id) use ($blade, $pokeApiService) {
    $pokemon = $pokeApiService->getSingle('pokemon', $id);
    echo $blade->make('show', compact('pokemon'));
});

$app->map('/search', function () use ($blade, $pokeApiService, $request) {
     $name = $request->get('name');
    $pokemon = $pokeApiService->getByName('pokemon', $name);
    echo $blade->make('show', compact('pokemon'));
});